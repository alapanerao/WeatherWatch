//
//  TableRow.h
//  FirshQatch
//
//  Created by Alap Anerao on 05/10/15.
//  Copyright © 2015 Alap Anerao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <WatchKit/WatchKit.h>

@interface TableRow : NSObject

@property IBOutlet WKInterfaceLabel *lblText;
@property IBOutlet WKInterfaceLabel *lblDate;
@property IBOutlet WKInterfaceImage *ivImage;

@end
