//
//  InterfaceController.h
//  WeatherDemo WatchKit Extension
//
//  Created by Alap Anerao on 07/10/15.
//  Copyright © 2015 Alap Anerao. All rights reserved.
//

#import <WatchKit/WatchKit.h>
#import <Foundation/Foundation.h>

@interface InterfaceController : WKInterfaceController

@end
