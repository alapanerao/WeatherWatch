//
//  ExtensionDelegate.h
//  WeatherDemo WatchKit Extension
//
//  Created by Alap Anerao on 07/10/15.
//  Copyright © 2015 Alap Anerao. All rights reserved.
//

#import <WatchKit/WatchKit.h>

@interface ExtensionDelegate : NSObject <WKExtensionDelegate>

@end
