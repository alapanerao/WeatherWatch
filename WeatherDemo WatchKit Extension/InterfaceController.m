//
//  InterfaceController.m
//  WeatherDemo WatchKit Extension
//
//  Created by Alap Anerao on 07/10/15.
//  Copyright © 2015 Alap Anerao. All rights reserved.
//

enum {
    kWeatherTypeClear = 800,
    kWeatherTypeScatteredClouds = 802,
    kWeatherTypeHaze= 721
};

#import "InterfaceController.h"
#import "TableRow.h"

#define kAPIBaseURL  @"http://api.openweathermap.org/data/2.5/weather?units=metric&"
#define kAPIForecastBaseURL @"http://api.openweathermap.org/data/2.5/forecast/daily?units=metric&"

@interface InterfaceController() <CLLocationManagerDelegate>

@property (weak, nonatomic) IBOutlet WKInterfaceTable   *tblList;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel   *lblLocationName;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel   *lblCuttentTemp;
@property (weak, nonatomic) IBOutlet WKInterfaceImage   *ivCurrentStatus;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel   *lblTmpMin;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel   *lblTmpMax;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel   *lblWind;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel   *lblHumidity;

@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) CLLocation *lastUserLocation;

@end


@implementation InterfaceController

- (void)awakeWithContext:(id)context {
    [super awakeWithContext:context];
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    if ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    [self.locationManager requestLocation];
    
    [self.tblList setNumberOfRows:0 withRowType:@"cell"];
}

- (void)willActivate {
    // This method is called when watch view controller is about to be visible to user
    [super willActivate];
}

- (void)didDeactivate {
    // This method is called when watch view controller is no longer visible
    [super didDeactivate];
}

#pragma mark - CLLocationManager delegate

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    NSLog(@"%@",[locations lastObject]);
    self.lastUserLocation = [locations lastObject];
    [self updateWeatherData];
}

- (void)locationManager:(CLLocationManager*)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    switch (status) {
        case kCLAuthorizationStatusNotDetermined: {
            [self.locationManager requestLocation];
        } break;
            
        case kCLAuthorizationStatusDenied: {
            
        } break;
            
        case kCLAuthorizationStatusAuthorizedWhenInUse: {
            [self.locationManager requestLocation];
        } break;
            
        case kCLAuthorizationStatusAuthorizedAlways: {
        } break;
        default:
            break;
    }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    NSLog(@"%@",error.userInfo);
}

#pragma mark - Custom methods

- (void)updateWeatherData {
    NSURLSession *session = [NSURLSession sharedSession];
    NSString *strURL = [NSString stringWithFormat:@"%@lat=%.0f&lon=%.0f",kAPIBaseURL,self.lastUserLocation.coordinate.latitude,self.lastUserLocation.coordinate.longitude];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:strURL] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error == nil) {
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            NSLog(@"%@", json);
            
            //Setting place name.
            NSString *strCityName = [json objectForKey:@"name"];
            NSString *strCountryCode = [[json objectForKey:@"sys"] objectForKey:@"country"];
            NSString *strTmp = [NSString stringWithFormat:@"%@,%@",strCityName,strCountryCode];
            [self.lblLocationName setText:strTmp];
            
            //Setting current weather image.
            NSDate *sunsetTime = [NSDate dateWithTimeIntervalSince1970:[[[json objectForKey:@"sys"] objectForKey:@"sunset"] integerValue]];
            NSDate *sunriseTime = [NSDate dateWithTimeIntervalSince1970:[[[json objectForKey:@"sys"] objectForKey:@"sunrise"] integerValue]];
            
            BOOL isNight = FALSE;
            if (![self isDate:[NSDate date] inRangeFirstDate:sunriseTime lastDate:sunsetTime]) {
                NSLog(@"Night");
                isNight = TRUE;
            } else {
                NSLog(@"Day");
            }
            
            NSString *weatherType = [[[json objectForKey:@"weather"] objectAtIndex:0] objectForKey:@"id"];
            if (weatherType.integerValue == kWeatherTypeClear) {
                [self.ivCurrentStatus setImage:isNight?[UIImage imageNamed:@"imgNightClear"]:[UIImage imageNamed:@"imgDaySunny"]];
            } else if (weatherType.integerValue < 600) {
                [self.ivCurrentStatus setImage:[UIImage imageNamed:@"imgRain"]];
            } else if (weatherType.integerValue >= 600 && weatherType.integerValue < 700) {
                [self.ivCurrentStatus setImage:isNight?[UIImage imageNamed:@"imgNightCloudy"]:[UIImage imageNamed:@"imgDayCloudy"]];
            } else {
                [self.ivCurrentStatus setImage:isNight?[UIImage imageNamed:@"imgNightCloudy"]:[UIImage imageNamed:@"imgDayCloudy"]];
            }
            
            //Setting current temperature.
            NSString *tmpStr = [[json objectForKey:@"main"] objectForKey:@"temp"];
            NSArray *arr = [tmpStr.description componentsSeparatedByString:@"."];
            NSString *temp = [NSString stringWithFormat:@"%@˚C",[arr objectAtIndex:0]];
            [self.lblCuttentTemp setText:temp.description];
            
            //Setting min/max temperature.
            NSString *tmpStrMin = [[json objectForKey:@"main"] objectForKey:@"temp_min"];
            NSArray *arrMin = [tmpStrMin.description componentsSeparatedByString:@"."];
            NSString *tempMin = [NSString stringWithFormat:@"%@˚C",[arrMin objectAtIndex:0]];
            [self.lblTmpMin setText:tempMin.description];
            
            NSString *tmpStrMax = [[json objectForKey:@"main"] objectForKey:@"temp_max"];
            NSArray *arrMax = [tmpStrMax.description componentsSeparatedByString:@"."];
            NSString *tempMax = [NSString stringWithFormat:@"%@˚C",[arrMax objectAtIndex:0]];
            [self.lblTmpMax setText:tempMax.description];
            
            //Setting wind and humidity.
            NSString *strWind = [NSString stringWithFormat:@"%@ mpc",[[json objectForKey:@"wind"] objectForKey:@"speed"]];
            
            [self.lblWind setText:strWind.description];
            
            NSString *strHumidity = [NSString stringWithFormat:@"%@ %",[[json objectForKey:@"main"] objectForKey:@"humidity"]];
            [self.lblHumidity setText:strHumidity.description];
            
            [self updateForecastData];
        }
    }];
    [dataTask resume];
}

- (void)updateForecastData {
    NSURLSession *session = [NSURLSession sharedSession];
    NSString *strURL = [NSString stringWithFormat:@"%@lat=%.0f&lon=%.0f",kAPIForecastBaseURL,self.lastUserLocation.coordinate.latitude,self.lastUserLocation.coordinate.longitude];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:strURL] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error == nil) {
            [self.tblList setNumberOfRows:5 withRowType:@"cell"];
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            NSArray *arrData = [json objectForKey:@"list"];
            NSLog(@"%@", json);
            
            for (NSInteger i = 0; i < self.tblList.numberOfRows; i++)
            {
                NSDictionary *dictWeatherData = [[arrData objectAtIndex:i] objectForKey:@"temp"];
                NSString *strMaxTmp = [dictWeatherData objectForKey:@"max"];
                NSString *strMinTmp = [dictWeatherData objectForKey:@"min"];
                
                //Setting min/max temperature.
                NSArray *arrMin = [strMinTmp.description componentsSeparatedByString:@"."];
                NSString *tempMin = [NSString stringWithFormat:@"%@˚",[arrMin objectAtIndex:0]];
                
                NSArray *arrMax = [strMaxTmp.description componentsSeparatedByString:@"."];
                NSString *tempMax = [NSString stringWithFormat:@"%@˚",[arrMax objectAtIndex:0]];
                
                NSString *strTmp =[NSString stringWithFormat:@"%@  %@",tempMax,tempMin];
                TableRow *row = (TableRow *)[self.tblList rowControllerAtIndex:i];
                [row.lblText setText:strTmp];
                
                
                //Setting current weather image.
                NSString *weatherType = [[[[arrData objectAtIndex:i] objectForKey:@"weather"] objectAtIndex:0] objectForKey:@"id"];
                if (weatherType.integerValue == kWeatherTypeClear) {
                    [row.ivImage setImage:[UIImage imageNamed:@"imgDaySunny"]];
                } else if (weatherType.integerValue < 600) {
                    [row.ivImage setImage:[UIImage imageNamed:@"imgRain"]];
                } else if (weatherType.integerValue >= 600 && weatherType.integerValue < 700) {
                    [row.ivImage setImage:[UIImage imageNamed:@"imgDayCloudy"]];
                } else {
                    [row.ivImage setImage:[UIImage imageNamed:@"imgDayCloudy"]];
                }
                
                NSDate *weatherDate = [NSDate dateWithTimeIntervalSince1970:[[[arrData objectAtIndex:i] objectForKey:@"dt"] integerValue]];
                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"dd"];
                NSString *strDay = [formatter stringFromDate:weatherDate];
                [row.lblDate setText:strDay];
            }
        }
    }];
    [dataTask resume];
}

- (BOOL)isDate:(NSDate *)date inRangeFirstDate:(NSDate *)firstDate lastDate:(NSDate *)lastDate {
    return [date compare:firstDate] == NSOrderedDescending &&
    [date compare:lastDate]  == NSOrderedAscending;
}

@end



