//
//  AppDelegate.h
//  WeatherDemo
//
//  Created by Alap Anerao on 07/10/15.
//  Copyright © 2015 Alap Anerao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

